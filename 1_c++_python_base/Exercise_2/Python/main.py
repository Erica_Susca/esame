# CRIPTAGGIO E DECRIPTAGGIO di un testo
# Viene fornito un testo (in un file .txt) da decriptare e una password passata da riga di comando
# che trasforma il testo in un testo criptato che non ha senso compiuto
import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText (inputFilePath):
    #apro il file di input
    file = open(inputFileName, 'r')
    #salvo in line la riga di file (unica) che ho letto
    line = file.readline()
    #chiudo il file
    file.close()
    # ritorno vero e la riga di file letta
    return True, line

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt (text, password):
    j = 0
    encryptedText = "" #inizializzo la stringa come vuota
    for c in text: # ciclo sulla lunghezza del testo
        encryptedText += chr(ord(c) + ord(password[j])) # salvo in encryptedText il carattere che corrisponde alla somma
        # dei 2 codici ASCII del testo e della password (funzione ord() ritorna il carattere ASCII)
        j += 1
        if j > (len(password)-1): # se j supera la lunghezza della password allora ricomincio a contare da 0
            j = 0
    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    # charPass = [ord(password[i]) for i in range(0, len(password))]
    # decryptedText = ""
    #
    # j = 0
    # for i in range(0, len(text)):
    #     temp = ord(text[i]) - charPass[j]
    #     if temp < 32:
    #         temp += 95
    #
    #     decryptedText += chr(temp)
    #
    #     if j == len(password) - 1:
    #         j = 0
    #     else:
    #         j += 1
    j = 0
    decryptedText = ""  # inizializzo la stringa come vuota
    for c in text:  # ciclo sulla lunghezza del testo
        decryptedText += chr(ord(c) - ord(password[j]))  # salvo in decryptedText il carattere che corrisponde alla differenza
        # dei 2 codici ASCII del testo e della password (funzione ord() ritorna il carattere ASCII)
        j += 1
        if j > (len(password) - 1):  # se j supera la lunghezza della password allora ricomincio a contare da 0
            j = 0

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]
    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)

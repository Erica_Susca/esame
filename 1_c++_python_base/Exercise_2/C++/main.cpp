// CRIPTAGGIO E DECRIPTAGGIO di un testo
// Viene fornito un testo (in un file .txt) da decriptare e una password passata da riga di comando
// che trasforma il testo in un testo criptato che non ha senso compiuto

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
    // messaggi di errore corrispondenti ai vari casi in cui va storto qualcosa
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl; //non viene fornita la password da riga di comando
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl; //non esiste il file di testo
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    //leggo da file mediante la funzione ifstream
    ifstream file;
    //la funzione open richiede in input il path del file da aprire
    file.open(inputFilePath);
    //leggo la prima riga del file mediante la funzione getline (nel file di testo in questione esiste solo una riga di testo)
    getline(file,text);
    //una volta terminate le operazioni, il file deve essere chiuso
    file.close();
  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    int n=text.length(); // in n inserisco la lunghezza del testo
    int v[n]; //dichiaro un vettore di interi lungo n
    for(int i=0;i<n;i++){
        v[i]=(int)(text[i]);// per ogni elemento del vettore faccio un casting da char a int di ogni carattere del testo
    }

    int lunpass=password.length();// in lunpass inserisco la lunghezza della password
    int pass[lunpass]; //dichiaro un vettore di interi lungo lunpass
    for(int i=0;i<lunpass;i++){
        pass[i]=(int)(password[i]);// per ogni elemento del vettore faccio un casting da char a int di ogni carattere della password
    }
    int i=0,j=0,k=0,vett[n];
    for(i=0;i<n;i++){
        vett[i]=0; //inizializzo il vettore vett (di lunghezza n) a 0
    }
    for(i=k;i<n;i++){ //k variabile che tiene in memoria il carattere del testo a cui sono arrivata con il criptaggio
            j=0; // ri-inizializzo la variabile che tiene in memoria il carattere della password a cui sono arrivata con il criptaggio,
            // quando j = lunpass-1 ricomincio a contare da j = 0
        while(j<lunpass && i<n){ // fino a quando non supero queste due lunghezze
            if(v[i]+pass[j]<256){ // se la somma dei caratteri ASCII e' < 256 allora
            vett[i]=pass[j]+v[i]; // in vett[i] inserisco la somma del carattere del testo piu' il carattere della password
            }
            else{
                vett[i]=pass[j]+v[i]-255+32; // altrimenti se sommando, viene superato il limite massimo, torno all'inizio e riprendo a contare
                // i primi 32 caratteri non sono stampabili quindi li devo escludere
            }
            j++;
            i++;
            k++;
        }
        i--; // decremento le 2 variabili perche' sono state incrementate una volta di troppo nel ciclo while
        k--;
    }

    char str[n];
    for(i=0;i<n;i++){
        str[i]=(char)vett[i];// ri-casto da interi corripondenti ai caratteri ASCII al carattere ASCII corriposndente
    }
    encryptedText=str; // la stringa criptata e' memorizzata in encryptedText
  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    //qui uguale alla funzione precedente solo che nel Decriptaggio scrivo le operazioni inverse
    int n=text.length();
    int vett[n];
    for(int i=0;i<n;i++){
        if((int)text[i]<0){ // se int corrispondente e' < 0 allora:
           vett[i]=(int)(text[i])+256; //casting da char a int +256
        }
        else{
           vett[i]=(int)(text[i]); //altrimenti casting da char a int
        }
    }

    int lun=password.length();
    int v[lun];
    for(int i=0;i<lun;i++){
        v[i]=(int)password[i];
    }
    char str[n];
    int i=0,j=0,k=0;
    for(i=k;i<n;i++){
            j=0;
        while(j<lun && i<n){
            if(vett[i]-v[j]>31){ // se il carattere e' stampabile allora:
            str[i]=static_cast<char>(vett[i]-v[j]); // ri-casto a carattere
            }
            else{
                str[i]=static_cast<char>(vett[i]-v[j]+32); // altrimenti ri-casto a carattre +32
            }
            j++;
            i++;
            k++;
        }
        i--;
        k--;
    }

   decryptedText=str;
  return  true;
}

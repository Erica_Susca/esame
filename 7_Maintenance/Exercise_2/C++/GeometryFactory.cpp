#include "GeometryFactory.h"

namespace GeometryFactoryLibrary {

  //questo metodo crea un oggetto di classe Polygon dato il vettore dei vertici
  int GeometryFactory::CreatePolygon(const vector<Vector2d>& vertices)
  {
    int polygonId = _polygons.size() + 1; //FIX si deve aggiungere +1, poichè da requirements l'enumerazione dei poligoni inizia da 1, non da 0

    //la funzione pair consente di combinare tra loro due oggetti che possono essere di tipo diverso
    //_polygons è un unordered map, un contenitore che memorizza elementi formati
    //dalla coppia valore chiave (int) e valore mappato. L'inserimento è O(1)
    _polygons.insert(pair<int, Polygon>(polygonId, Polygon()));

    //anche _polygonVertices _polygonEdges sono unordered maps
    _polygonVertices.insert(pair<int, vector<const Point*>>(polygonId, vector<const Point*>()));
    _polygonEdges.insert(pair<int, vector<const Segment*>>(polygonId, vector<const Segment*>()));

    //si ricorre all'uso della referenza così da poterne modificare il valore
    vector<const Point*>& polygonVertices = _polygonVertices[polygonId];
    vector<const Segment*>& polygonEdges = _polygonEdges[polygonId];

    unsigned int numVertices = vertices.size();

    //procediamo ora riempiendo tutti i vertici
    //la funzione reserve dispone tante allocazioni di memoria quanti sono i numVertices
    polygonVertices.reserve(numVertices);
    for (unsigned int v = 0; v < numVertices; v++)
    {
      const Vector2d& vertex = vertices[v];

      _points.push_back(Point());

      //anche in questo caso ricorriamo alla referenza, così da poter modificare il valore
      Point& point = _points.back();
      point.X = vertex.X();
      point.Y = vertex.Y(); //FIX Secondo errore: era presente vertex.X(), anzichè vertex.Y()

      polygonVertices.push_back(&point);
    }

    //riempio tutti i lati, usando ancora la funzione reserve
    polygonEdges.reserve(numVertices);
    for (unsigned int e = 0; e < numVertices; e++)
    {
      const Point& from = *polygonVertices[e]; //"da" indica il punto di partenza
      const Point& to = *polygonVertices[(e + 1) % numVertices]; //FIX Terzo errore: c'era (e + 2) invece di (e + 1)

      //segments è una lista
      _segments.push_back(Segment(from, to));
      //la funzione back ritorna una referenza all'ultimo elemento della lista
      Segment& segment = _segments.back();
      //ed aggiungo di volta in volta segment a polygonEdges
      polygonEdges.push_back(&segment);
    }
    return polygonId; //ritorno l'Id del poligono
  }

  //questo metodo restituisce il poligono di riferimento corrispondente all'identificatore univoco
  const Polygon& GeometryFactory::GetPolygon(const int& polygonId)
  {
    //utilizzo la funzione find per individuare il valore
    const auto& polygonIterator = _polygons.find(polygonId);

    if (polygonIterator == _polygons.end()) //FIX Quarto errore: era != ma a noi serve ==
      throw runtime_error("Polygon not found");

    return polygonIterator->second; //la funzione find ritorna un iteratore al primo elemento dell'unordered map _polygons
  }

  int GeometryFactory::GetPolygonNumberVertices(const int& polygonId)
  {
      //uso la funzione find per trovare l'Id del poligono
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end()) //cioè non punta a nulla (non ho trovato l'Id corrispondente)
      throw runtime_error("Polygon not found");

    //inserisco in polygonVertices il vettore di punti a cui punta polygonIterator
    const vector<const Point*>& polygonVertices = polygonIterator->second;
    return polygonVertices.size(); //ritorno il numero di vertici del poligono
  }

  const Point& GeometryFactory::GetPolygonVertex(const int& polygonId, const int& vertexPosition)
  {
      //uso la funzione find per trovare l'Id del poligono
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end()) //cioè non punta a nulla (non ho trovato l'Id corrispondente)
      throw runtime_error("Polygon not found");

    //inserisco in polygonVertices il vettore di punti a cui punta polygonIterator
    const vector<const Point*>& polygonVertices = polygonIterator->second;

    if ((unsigned int)vertexPosition >= polygonVertices.size())
      throw runtime_error("Vertex not found"); //FIX Quinto errore: era "foudn" anzichè "found"

    return *polygonVertices[vertexPosition]; // ritorno la referenza al punto che corrisponde al vertice in posizione vertexPosition
  }

  const Segment& GeometryFactory::GetPolygonEdge(const int& polygonId, const int& edgePosition)
  {
      //uso la funzione find per trovare l'Id del poligono
    const auto& polygonIterator = _polygonEdges.find(polygonId);

    if (polygonIterator == _polygonEdges.end()) //cioè non punta a nulla (non ho trovato l'Id corrispondente)
      throw runtime_error("Polygon not found");

    //inserisco in polygonEdges il vettore di segmenti a cui punta polygonIterator
    const vector<const Segment*>& polygonEdges = polygonIterator->second;

    if ((unsigned int)edgePosition >= polygonEdges.size())
      throw runtime_error("Edge not found");

    return *polygonEdges[edgePosition]; // ritorno la referenza al segmento che corrisponde al lato in posizione edgePosition
  }

  Vector2d::Vector2d(const double& x, const double& y) //costruttore della classe Vector2d
  {
    _x = x;
    _y = y;
  }

  Segment::Segment(const Point& from, const Point& to) : From(from), To(to) { } //costruttore della classe Segment

}

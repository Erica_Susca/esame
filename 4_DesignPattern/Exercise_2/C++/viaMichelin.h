#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  //classe BusStation
  class BusStation : public IBusStation {

  private:
        string _busFilePath;
        int _numberBus;
        vector<Bus> _buses; //vettore dei bus

  public:
      BusStation(const string& busFilePath) {_busFilePath = busFilePath; } //costruttore della classe BusStation

      void Load();
      int NumberBuses() const  {  return _numberBus; }
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {
  private:
      string _mapFilePath;
            int _numberBusStops;
            int _numberStreets;
            int _numberRoutes;
            vector<BusStop> _busStops; //vettore di BusStop
            vector<Street> _streets; // vettore si strade
            vector<Route> _routes; //vettore di percorsi
            vector<int> _streetsFrom; //vettore di interi
            vector<int> _streetsTo;//vettore di interi
            vector<vector<int>> _routeStreets; // vettore di vettori di interi

      void Reset();

  public:
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath;  } //costruttore della classe MapData
      void Load();
      int NumberRoutes() const { return _numberRoutes; }
      int NumberStreets() const { return _numberStreets; }
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed; //attributo statico

    private:
      // referenze alle due interfacce IMapData e IBusStation
        const IMapData& _mapData;
        const IBusStation& _busStation;
    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) : _mapData(mapData), _busStation(busStation) { } //costruttore della classe RoutePlanner

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
  private:
      //referenza all'interfaccia IMapData
        const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData) : _mapData(mapData) { } //costruttore della classe MapViewer
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H

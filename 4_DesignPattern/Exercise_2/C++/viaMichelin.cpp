# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load() //caricamento di tutti i dati di BusStation
{
    // Reset station
        _numberBus = 0;
        _buses.clear();

        // Open File
        ifstream file;
        file.open(_busFilePath.c_str());

        if (file.fail())
          throw runtime_error("Something goes wrong");

        // Load buses
        try {
          string line;
          // number buses
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertN;
          convertN.str(line);
          convertN >> _numberBus;
          // buses
          getline(file, line); // Skip Comment Line
          getline(file, line); // Skip Comment Line

          // Fill Buses
          _buses.resize(_numberBus); //riservo memoria pari a _numberBus
          for (int b = 0; b < _numberBus; b++)
                {
                  // per ogni riga (ogni riga corrisponde ad un bus) leggo la linea e
                  getline(file, line);
                  istringstream converter;
                  //converto la linea e inserisco elementi trovati in _buses
                  converter.str(line);
                  converter >> _buses[b].Id >> _buses[b].FuelCost;
                }

        } catch (exception) {
          _numberBus = 0;
          _buses.clear();

          throw runtime_error("Something goes wrong");
        }

        // Close File
        file.close();
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus > _numberBus) //se l'id del bus e' > del numero di bus allora:
          throw runtime_error("Bus " + to_string(idBus) + " does not exists"); //lancio messaggio

    return _buses[idBus - 1];// altrimenti ritorno il bus alla posizione (idBus -1) poiche' la numerazione parte da 0
}

//funzione che re-inizializza (resetta) tutti gli attributi
void MapData::Reset()
{
    _numberRoutes = 0;
        _numberStreets = 0;
        _numberBusStops = 0;
        _busStops.clear();
        _streets.clear();
        _routes.clear();
        _streetsFrom.clear();
        _streetsTo.clear();
        _routeStreets.clear();
}

void MapData::Load() //caricamento di tutti i dati di MapData
{
    // Reset map
        Reset();

        // Open File
        ifstream file;
        file.open(_mapFilePath.c_str());

        if (file.fail())
          throw runtime_error("Something goes wrong");

        // Load
        try {
          string line;

          // Get busStops
          // numberBusStops
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertBusStops;
          convertBusStops.str(line);
          convertBusStops >> _numberBusStops;

          // BusStops
          getline(file, line); // Skip Comment Line
          _busStops.resize(_numberBusStops); // riservo memoria pari a _numberBusStops
          for (int b = 0; b < _numberBusStops; b++)
          {
            // per ogni riga (ogni riga corrisponde ad una busStop) leggo la linea e
            getline(file, line);
            istringstream converter;
            //converto la linea e inserisco elementi trovati in _busStop
            converter.str(line);
            converter >> _busStops[b].Id >> _busStops[b].Name >> _busStops[b].Latitude >> _busStops[b].Longitude;
          }

          // Get streets
          // numberStreets
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertStreets;
          convertStreets.str(line);
          convertStreets >> _numberStreets;

          // Streets
          getline(file, line); // Skip Comment Line
          _streets.resize(_numberStreets);
          _streetsFrom.resize(_numberStreets);
          _streetsTo.resize(_numberStreets);

          for (int s = 0; s < _numberStreets; s++)
          {
            // per ogni riga (ogni riga corrisponde ad una strada) leggo la linea e
            getline(file, line);
            istringstream converter;
            //converto la linea e inserisco elementi trovati in _streets
            converter.str(line);
            converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
          }

          // Get routes
          // numberRoutes
          getline(file, line); // Skip Comment Line
          getline(file, line);
          istringstream convertRoutes;
          convertRoutes.str(line);
          convertRoutes >> _numberRoutes;

          // Routes
          getline(file, line); // Skip Comment Line
          _routes.resize(_numberRoutes);
          _routeStreets.resize(_numberRoutes);

          for (int r = 0; r < _numberRoutes; r++)
          {
              // per ogni riga (ogni riga corrisponde ad un percorso) leggo la linea e
            getline(file, line);
            istringstream converter;
            //converto la linea e inserisco elementi trovati in _routes
            converter.str(line);
            // Id --- numero di strade legate a quell'Id --- Id delle strade legate all'Id iniziale
            converter >> _routes[r].Id >> _routes[r].NumberStreets;

            _routeStreets[r].resize(_routes[r].NumberStreets);

            for (int s = 0; s < _routes[r].NumberStreets; s++) //ciclo sul numero di strade legate all'Id iniziale
              converter >> _routeStreets[r][s]; //inserisco gli elementi trovati in _routeStreets (vector<vector<int>>)
          }

          // Close File
          file.close();
        } catch (exception) {
          Reset();

          throw runtime_error("Something goes wrong");
        }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if (idRoute > _numberRoutes)
         throw runtime_error("Route " + to_string(idRoute) + " does not exists");

       const Route& route = _routes[idRoute - 1];
       if (streetPosition >= route.NumberStreets)
         throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");
       // prendo l'idStreet
       int idStreet = _routeStreets[idRoute - 1][streetPosition];
       // e lo uso per prendere la street alla posizione idStreet - 1
       return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if (idRoute > _numberRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    //prendo il percorso alla posizione idRoute - 1
    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    //prendo la strada alla posizione idStreet - 1
    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    //prendo la fermata alla posizione idFrom - 1
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

        //idFrom e' l'Id della strada da cui parto col bus
        int idFrom = _streetsFrom[idStreet - 1];

        return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    //prendo la fermata alla posizione idTo - 1
    if (idStreet > _numberStreets)
         throw runtime_error("Street " + to_string(idStreet) + " does not exists");

       //idTo e' l'Id della strada a cui arrivo col bus
       int idTo = _streetsTo[idStreet - 1];

       return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    //prendo la fermata alla posizione idBusStop - 1
    if (idBusStop > _numberBusStops)
          throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    // funzione che calcola (dato l'idroute del percorso) il tempo totale impiegato a percorrere la route in posizione idRoute
    const Route& route = _mapData.GetRoute(idRoute);
    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++){ //ciclo su tutte le strade percorse
        travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;
    }
    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    //funzione che calcola il costo del percorso in base al tempo di viaggio,costo benzina e velocità media
    const Route& route = _mapData.GetRoute(idRoute);
    const Bus& bus = _busStation.GetBus(idBus);
    int totalTravelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++){
        totalTravelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime * bus.FuelCost * BusAverageSpeed;
    }
    totalTravelTime /= 3600; //divido per 3600 poichè il traveltime è espresso in secondi
    return totalTravelTime;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    //funzione che ritorna una stringa che permette di visualizzare i dettagli su ogni percorso:
    // id del percorso e poi tutte le fermate fino alla destinazione finale
    const Route& route = _mapData.GetRoute(idRoute);

       int s = 0;
       ostringstream routeView;
       routeView<< to_string(idRoute)<< ": ";
       for (s = 0; s < route.NumberStreets - 1; s++)
       {
         int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
         string from = _mapData.GetStreetFrom(idStreet).Name;
         routeView << from<< " -> ";
       }

       int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
       string from = _mapData.GetStreetFrom(idStreet).Name;
       string to = _mapData.GetStreetTo(idStreet).Name;
       routeView << from<< " -> "<< to;

       return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    //funzione che ritorna una stringa che permette di visualizzare fermata di partenza e fermata di arrivo dato l'id della strada
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    //funzione che ritorna una stringa che permette di visualizzare nome,latitudine e longitudine di una fermata,dato l'id della fermata
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}



}

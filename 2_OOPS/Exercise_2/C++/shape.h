#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
  public:
       double X;
       double Y;
    public:
      Point(const double& x,
            const double& y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
      protected:
      Point centro;
      int A;
      int B;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const {return Ellipse::Area();}
  };


  class Triangle : public IPolygon
  {
    public:
      Point a;
      Point b;
      Point c;
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3) ;

      double Area() const ;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      int lato;
      TriangleEquilateral(const Point& p1,
                          const int& edge) ;

      double Area() const ;
  };

  class Quadrilateral : public IPolygon
  {
    public:
       Point a;
       Point b;
       Point c;
       Point d;
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
       Point a;
       Point b;
       Point c;
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4) ;

      double Area() const ;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      int b;
      int h;
      Rectangle(const Point& p1,
                const int& base,
                const int& height) ;

      double Area() const ;
  };

  class Square: public Rectangle
  {
    public:
      int lato;
      Square(const Point& p1,
             const int& edge) ;

      double Area() const {return Rectangle::Area(); }
  };
}

#endif // SHAPE_H

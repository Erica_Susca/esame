// calcolo area di diversi poligoni
#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

Point::Point(const Point &point) //costruttore del punto
{
    X=point.X;
    Y=point.Y;
}
Ellipse::Ellipse(const Point &center, const int &a, const int &b):centro(center)
{
    A = a;
    B = b;
}

double Ellipse::Area() const
{
    return A*B*M_PI;
}

Circle::Circle(const Point &center, const int &radius):Ellipse(center,radius,radius)
{

}
Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3):a(p1),b(p2),c(p3)
{
  //costruttore del triangolo, che inizializza gli attributi p1, p2, p3
}

double Triangle::Area() const
{
    double n1 = sqrt((a.X-b.X)*(a.X-b.X)+(a.Y-b.Y)*(a.Y-b.Y)); // lunghezza lato 1
    double n2 = sqrt((a.X-c.X)*(a.X-c.X)+(a.Y-c.Y)*(a.Y-c.Y)); // lunghezza lato 2
    double n3 = sqrt((c.X-b.X)*(c.X-b.X)+(c.Y-b.Y)*(c.Y-b.Y)); // lunghezza lato 3
    double p = 0.5*(n1+n2+n3); //calcolo semiperimetro
    return sqrt(p*(p-n1)*(p-n2)*(p-n3)); //area data dalla formula di Erone
}
TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):Triangle(p1,p1,p1) //costruttore triangolo equilatero
{
    lato = edge;
}

double TriangleEquilateral::Area() const
{
    return (sqrt(3)/4)*lato*lato;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4):a(p1),b(p2),c(p3),d(p4)
{
 //costruttore del quadrilatero
}

double Quadrilateral::Area() const
{
    //suddivido il quadrilatero di partenza in 2 sotto-triangoli
    double n1 = sqrt((a.X-b.X)*(a.X-b.X)+(a.Y-b.Y)*(a.Y-b.Y));
    double n2 = sqrt((b.X-c.X)*(b.X-c.X)+(b.Y-c.Y)*(b.Y-c.Y));
    double n3 = sqrt((c.X-d.X)*(c.X-d.X)+(c.Y-d.Y)*(c.Y-d.Y));
    double n4 = sqrt((d.X-a.X)*(d.X-a.X)+(d.Y-a.Y)*(d.Y-a.Y));
    double n5 = sqrt((d.X-b.X)*(d.X-b.X)+(d.Y-b.Y)*(d.Y-b.Y));
    double p1 = 0.5*(n1+n4+n5);
    double r1=sqrt(p1*(p1-n1)*(p1-n4)*(p1-n5)); //area triangolo 1
    double p2 = 0.5*(n2+n3+n5);
    double r2=sqrt(p2*(p2-n2)*(p2-n3)*(p2-n5)); // area triangolo 2
    return r1+r2; //area quadrilatero = somma delle 2 aree dei 2 triangoli
}
Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):Quadrilateral(a,b,c,a),a(p1),b(p2),c(p4)
{
 // costruttore parallelogramma
}

double Parallelogram::Area() const
{
    double n1 = sqrt((a.X-b.X)*(a.X-b.X)+(a.Y-b.Y)*(a.Y-b.Y));
    double n2 = sqrt((a.X-c.X)*(a.X-c.X)+(a.Y-c.Y)*(a.Y-c.Y));
    double n3 = sqrt((c.X-b.X)*(c.X-b.X)+(c.Y-b.Y)*(c.Y-b.Y));
    double p = 0.5*(n1+n2+n3);
    return 2*(sqrt(p*(p-n1)*(p-n2)*(p-n3))); // calcolo area parallelogramma applicando la formula di Erone a uno dei 2 triangoli
    // (essendo triangoli uguali basta moltiplicare l'area di uno dei due per 2)
}
Rectangle::Rectangle(const Point &p1, const int &base, const int &height):Quadrilateral(p1,p1,p1,p1) //costruttore del rettangolo
{
    b = base;
    h = height;
}

double Rectangle::Area() const
{
    return b*h; //area del rettangolo
}
Square::Square(const Point &p1, const int &edge):Rectangle(p1,edge,edge) //costruttore del quadrato
{
    lato = edge;
}

}

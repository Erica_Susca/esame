from math import sqrt, pi

# classe del punto
class Point:
    def __init__(self, x: float, y: float): #costruttore del punto
        self.x = x
        self.y = y


class IPolygon: #interfaccia del poligono
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int): #costruttore dell'ellisse
        self.Point = center
        self.a = a
        self.b = b

    def area(self):
        return self.a * self.b * pi #calcolo area ellisse


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int): #costruttore del cerchio
        super().__init__(center, radius, radius) #calcolo area cerchio
        # super è una funzione che serve per dare alle classi figlie i metodi del padre


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.a = p1
        self.b = p2
        self.c = p3

    def area(self):
        n1 = sqrt((self.a.x - self.b.x) * (self.a.x - self.b.x) + (self.a.y - self.b.y) * (self.a.y - self.b.y))#calcolo lato 1
        n2 = sqrt((self.a.x - self.c.x) * (self.a.x - self.c.x) + (self.a.y - self.c.y) * (self.a.y - self.c.y))#calcolo lato 2
        n3 = sqrt((self.c.x - self.b.x) * (self.c.x - self.b.x) + (self.c.y - self.b.y) * (self.c.y - self.b.y))#calcolo lato 3
        p = 0.5*(n1+n2+n3) #calcolo semiperimetro
        return sqrt(p*(p-n1)*(p-n2)*(p-n3)) #calcolo area triangolo con formula di Erone


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int): #costruttore del triangolo equilatero
        self.Point = p1
        self.edge = edge #edge è una variabile che identifica il lato

    def area(self):
        return sqrt(3) / 4 * self.edge * self.edge #calcolo area triangolo equilatero


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point): #costruttore del quadrilatero
        self.a = p1
        self.b = p2
        self.c = p3
        self.d = p4

    def area(self):
        #suddivido il quadrilatero di partenza in 2 sotto-triangoli
        n1 = sqrt((self.a.x - self.b.x) * (self.a.x - self.b.x) + (self.a.y - self.b.y) * (self.a.y - self.b.y))
        n2 = sqrt((self.b.x - self.c.x) * (self.b.x - self.c.x) + (self.b.y - self.c.y) * (self.b.y - self.c.y))
        n3 = sqrt((self.c.x - self.d.x) * (self.c.x - self.d.x) + (self.c.y - self.d.y) * (self.c.y - self.d.y))
        n4 = sqrt((self.d.x - self.a.x) * (self.d.x - self.a.x) + (self.d.y - self.a.y) * (self.d.y - self.a.y))
        n5 = sqrt((self.d.x - self.b.x) * (self.d.x - self.b.x) + (self.d.y - self.b.y) * (self.d.y - self.b.y))
        p1 = 0.5 * (n1 + n4 + n5)
        r1 = sqrt(p1 * (p1 - n1) * (p1 - n4) * (p1 - n5))
        p2 = 0.5 * (n2 + n3 + n5)
        r2 = sqrt(p2 * (p2 - n2) * (p2 - n3) * (p2 - n5))
        #calcolo ora l'area del quadrilatero come somma dei 2 sotto-triangoli ottenuti prima
        return r1+r2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point): #costruttore del parallelogramma
        self.a = p1
        self.b = p2
        self.c = p4

    def area(self):
        n1 = sqrt((self.a.x - self.b.x) * (self.a.x - self.b.x) + (self.a.y - self.b.y) * (self.a.y - self.b.y))
        n2 = sqrt((self.a.x - self.c.x) * (self.a.x - self.c.x) + (self.a.y - self.c.y) * (self.a.y - self.c.y))
        n3 = sqrt((self.c.x - self.b.x) * (self.c.x - self.b.x) + (self.c.y - self.b.y) * (self.c.y - self.b.y))
        p = 0.5 * (n1 + n2 + n3)
        return 2 * (sqrt(p * (p - n1) * (p - n2) * (p - n3))) #calcolo area parallelogramma


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int): #costruttore rettangolo
        self.Point = p1
        self.base = base
        self.height = height

    def area(self):
        return self.base*self.height #calcolo area rettangolo


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int): #costruttore quadrato
        super().__init__(p1, edge, edge)
        # super è una funzione che serve per dare alle classi figlie i metodi del padre

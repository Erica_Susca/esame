from unittest import TestCase
import math

import shape as shapeLibrary


class TestEllipse(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Ellipse(shapeLibrary.Point(1.2, 2.5),
                                                 12,
                                                 15).area() - 565.4866776461628) < 1e-6)


class TestCircle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Circle(shapeLibrary.Point(1.2, 2.5),
                                                17).area() - 907.9202768874503) < 1e-6)


class TestTriangle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Triangle(shapeLibrary.Point(2.8, 3.9),
                                                  shapeLibrary.Point(3.9, 1.8),
                                                  shapeLibrary.Point(3.1, 2.7)).area() - 0.345) < 1e-6)


class TestTriangleEquilateral(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.TriangleEquilateral(shapeLibrary.Point(1.2, 1.7),
                                                             16).area() - 110.851251684) < 1e-6)


class TestQuadrilateral(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Quadrilateral(shapeLibrary.Point(-3, 1),
                                                       shapeLibrary.Point(1, -2),
                                                       shapeLibrary.Point(3, 2),
                                                       shapeLibrary.Point(-1, 4)).area() - 19.0) < 1e-6)


class TestParallelogram(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Parallelogram(shapeLibrary.Point(3, 1),
                                                       shapeLibrary.Point(10, 1),
                                                       shapeLibrary.Point(4, 3)).area() - 14.0) < 1e-6)


class TestRectangle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Rectangle(shapeLibrary.Point(3, 1),
                                                   7,
                                                   3).area() - 21.0) < 1e-6)


class TestSquare(TestCase):
    def test_area(self):
        self.assertTrue(abs(shapeLibrary.Square(shapeLibrary.Point(3, 1),
                                                7).area() - 49.0) < 1e-6)

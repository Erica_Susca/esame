#include "Intersector2D1D.h"
#include <vector>

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    //defisco le opportune tolleranze, rispettivamente per l'intersezione e per il parallelismo
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoInteresection;
    intersectionParametricCoordinate = 0;
}
Intersector2D1D::~Intersector2D1D()
{

}

Vector3d Intersector2D1D::IntersectionPoint()
{
    // se non vi è intersezione oppure non è ancora stata calcolata, lanciamo un messaggio di errore
    if (IntersectionType() != PointIntersection)
            throw runtime_error("There is no intersection");
    return *lineOriginPointer + intersectionParametricCoordinate * *lineTangentPointer;
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal; //indica il vettore normale al piano
    planeTranslationPointer = &planeTranslation; //indica il parametro di traslazione (d) del piano

  return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin; //origine della retta
    lineTangentPointer = &lineTangent; //vettore tangente alla retta

  return;
}
// ***************************************************************************
//calcolo diretto delle intersezioni
bool Intersector2D1D::ComputeIntersection()
{
    intersectionType = NoInteresection; //setto di default no intersezione
    bool intersection = true;
       // M_x0 indica il parametro di traslazione (d) del piano
       const double M_x0 = *planeTranslationPointer;

       // My_0 indica il prodotto scalare tra il vettore normale al piano (planeNormalPointer) e il vettore dato dal punto di origine della retta (lineOriginPointer)
       const double My_0 = lineOriginPointer->x() * planeNormalPointer->x()+lineOriginPointer->y() * planeNormalPointer->y()+lineOriginPointer->z() * planeNormalPointer->z();

       // Mt indica il prodotto scalare tra la normale al piano (planeNormalPointer) ed il vettore tangente alla retta (lineTangentPointer)
       const double Mt = lineTangentPointer->x() * planeNormalPointer->x()+lineTangentPointer->y() * planeNormalPointer->y()+lineTangentPointer->z() * planeNormalPointer->z();

       //se la retta ed il piano sono fra loro paralleli (Mt ->0)
       if(abs(Mt) <  toleranceParallelism){
           //e se la retta è contenuta nel piano
           if(abs(M_x0 - My_0) < toleranceIntersection){ //traslazione_piano (d) - (normale al piano).dot(origine_della_retta) = 0
               intersectionType = Coplanar; //ho tipo di intersezione coplanare
           }
           else{ //se la retta non è contenuta nel piano
               intersectionType = NoInteresection; //allora non ho intersezione
           }
           intersection = false;//setto l'intersezione a falso
       }
       else{ //altrimenti
          intersectionParametricCoordinate = (M_x0 - My_0)/Mt; // il valore dell'ascissa curvilinea  =
          // rapporto tra (prodotto scalare tra la normale al piano e la retta che congiunge 2 punti del piano)
          // ed il prodotto scalare tra la normale al piano ed il vettore tangente alla retta
          intersectionType = PointIntersection; // c'è intersezione tra la retta ed il piano
       }

  return intersection;
}

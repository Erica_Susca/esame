#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    //definiamo le due rispettive tolleranze
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;

    //definiamo la matrice avente come prime 2 righe i vettori normali ai piani N1 e N2
    //la terza riga ha invece N1xN2, che è il vettore tangente alla linea di intersezione
    matrixNomalVector.setZero(); //settiamo inizialmente a zero tutte le sue entrate

    //definiamo ora il vettore contenente i parametri di traslazione dei piani (d1, d2, d3)
    rightHandSide.setZero(); //settiamo inizialmente a zero anche questo vettore
    //setto di default no intersezione
    intersectionType = Intersector2D2D::NoInteresection;

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
//Considero il primo piano dell'intersezione
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    rightHandSide[0]=planeTranslation;
    matrixNomalVector.row(0)=planeNormal/planeNormal.norm();

  return;
}

// ***************************************************************************
//Considero il secondo piano dell'intersezione
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    rightHandSide[1]=planeTranslation;
    matrixNomalVector.row(1)=planeNormal/planeNormal.norm();

  return ;
}

// ***************************************************************************
//Calcolo direttamente l'intersezione tra i 2 piani
bool Intersector2D2D::ComputeIntersection()
{
    intersectionType = NoInteresection;//setto di default che i 2 piani non si intersecano
    bool intersection = true;
    //definiamo il vettore tangente alla linea di intersezione mediante N1xN2
    tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
    //calcolo (N1xN2) e lo inserisco nella 3 riga di matrixNomalVector dopo averlo normalizzato
    matrixNomalVector.row(2)=tangentLine/tangentLine.norm();
    rightHandSide[2]=0;// inizializzo la 3 entrata di rightHandSide a 0
    double parallelism = tangentLine.norm(); //definisco parallelism come la norma del vettore tangente alla linea di intersezione
    if(parallelism < toleranceParallelism){ // se parallelism è minore della tolleranza settata
        if(abs(rightHandSide[0] - rightHandSide[1]) < toleranceIntersection){ //e se la differenza dei parametri di traslazione (d1 e d2)
            //dei due piani N1 ed N2 è minore della tolleranza,allora i due piani sono coincidenti
            intersectionType = Coplanar;
            intersection = false;
        }
        else{
            intersection = false;//altrimenti i due piani non si intersecano
        }
    }
    else{ //se i due piani non sono paralleli allora:
        //setto il tipo di intersezione a LineIntersection
        intersectionType = LineIntersection;
        // risolvo il sistema sfruttando la fattorizzazione PALU, data la matrice matrixNomalVector e il vettore rightHandSide
        pointLine = matrixNomalVector.fullPivLu().solve(rightHandSide);
        tangentLine = matrixNomalVector.row(2);
    }

  return intersection;
}

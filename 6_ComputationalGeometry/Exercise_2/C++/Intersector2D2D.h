#ifndef INTERSECTOR2D2D_HPP
#define INTERSECTOR2D2D_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

class Intersector2D2D;
//calcolo intersezione fra 2 piani
class Intersector2D2D
{
  public:
    //diversi tipi di intersezione
    enum TypeIntersection
    {
      NoInteresection = 0,
      Coplanar = 1,
      LineIntersection = 2
    };

  protected:
    TypeIntersection intersectionType;
    // definizione delle tolleranze per il parallelismo e l'intersezione
    double toleranceParallelism;
    double toleranceIntersection;

    // parametri per identificare la linea di intersezione
    Vector3d pointLine;
    Vector3d tangentLine;
    // righthand è un vettore contenente i parametri di traslazione del piano
    Vector3d rightHandSide;
    // matrixNomalVector è una matrice contenente N1,N2 e (N1xN2)
    Matrix3d matrixNomalVector;

  public:
    Intersector2D2D();
    ~Intersector2D2D();

    void SetTolleranceIntersection(const double& _tolerance) { toleranceIntersection = _tolerance; }
    void SetTolleranceParallelism(const double& _tolerance) { toleranceParallelism = _tolerance; }

    const TypeIntersection& IntersectionType() const { return intersectionType; }
    const double& ToleranceIntersection() const { return toleranceIntersection; }
    const double& ToleranceParallelism() const { return toleranceParallelism; }
    const Vector3d& PointLine() const { return pointLine; }
    const Vector3d& TangentLine() const { return tangentLine; }

    void SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation);
    void SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation);
    bool ComputeIntersection();
};

#endif //INTERSECTOR2D2D_HPP


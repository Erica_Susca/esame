#include "Pizzeria.h"

namespace PizzeriaLibrary {


Ingredient::Ingredient(const string &name, const string &description, const int &price) //costruttore della classe ingrediente
{
    Name = name;
    Price = price;
    Description = description;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    try {
        FindIngredient(name);//ricerca del nome dell'ingrediente
    }
    catch (const exception& exception) {
        ingredients.push_back(Ingredient(name,description,price)); //se non ho trovato alcuna corrispondenza allora
        //aggiungo l'ingrediente al mio vettore di ingredienti
        return;
    }
    throw runtime_error("Ingredient already inserted"); // altrimenti lancio il messaggio: ingrediente gia' inserito

}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    unsigned int numIngredients = ingredients.size(); //inizializzo il numero di ingredienti
    for(unsigned int i = 0;i < numIngredients; i++){//ciclo sul vettore di ingredienti
        if(ingredients[i].Name == name){ //se ho trovato una corrispondenza peril nome dell'ingrediente:
            return ingredients[i];//ritorno l'ingrediente i-esimo
        }
    }
        throw runtime_error("Ingredient not found");//altrimenti lancio messaggio:ingrediente non trovato

}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    try {
        FindPizza(name);//cerco il nome della mia pizza
    }
    catch (const exception& exception) { //se non ho trovato corrispondenze per il nome della pizza
        Pizza newPizza;//costruisco l'istanza pizza
        newPizza.Name = name; //assegno alla nuova pizza il nome passato in input dalla funzione
        for(unsigned int i = 0;i < ingredients.size();i++){ // ciclo sugli ingredienti
            newPizza.Ingredients.push_back(FindIngredient(ingredients[i])); // ogni volta che trovo un ingrediente
            //aggiungo l'ingrediente trovato al vettore degli ingredienti che servono per creare la pizza
        }
        pizzas.push_back(newPizza);//aggiungo la nuova pizza al vettore di pizze
        return;
    }
    throw runtime_error("Pizza already inserted");//se ho trovato una corrsipondenza per il nome della pizza lancio il messaggio
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned intnumPizzas = pizzas.size(); //inizializzo il numero di pizze
    for(unsigned int i = 0;i < intnumPizzas; i++){ //ciclo sul numero di pizze
        if(pizzas[i].Name == name){ //se ho trovato una corrispondenza per il nome della pizza allora:
            return pizzas[i];//ritorno la pizza i-esima del vettore di pizze
        }
    }
    throw runtime_error("Pizza not found");//altrimenti lancio il messaggio
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    unsigned int n = pizzas.size();//inizializzo il numero di pizze dell'ordine
    if(n==0){
        throw runtime_error("Empty order");//se numero di pizze ordinate e' 0 lancio il messaggio
    }
    else{
        Order newOrder; //altrimenti creo un nuovo ordine
        for(unsigned int i=0;i<n;i++){ //ciclo sul numero di pizze ordinate
            newOrder.Pizzas.push_back(FindPizza(pizzas[i])); // ogni volta che trovo una pizza
            //aggiungo la pizza trovata al vettore delle pizze che servono per creare l'ordine
        }
        orders.push_back(newOrder); //aggiungo il nuovo ordine al vettore di ordini
    }
    int numOrder = 999+orders.size(); //il numero di ordini e' dato dalla lunghezza del vettore orders + 999
    //poiche' gli ordini partono da 1000
    return numOrder; //ritorno il numero di ordini
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    int n = numOrder-1000; //dichiaro il numero dell'ordine
    if(n < (int)orders.size()){ //se il numero dell'ordine e' < della taglia del vettore degli ordini allora:
        return orders[n]; //ritorno l'ordine n-esimo
    }
    throw runtime_error("Order not found");//altrimenti lancio il messaggio
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
   string receipt;
   Order _order;
   try {
       _order = FindOrder(numOrder); //cerco l'ordine
   }  catch (const exception& exception) {
       throw runtime_error("Order not found");// se non lo trovo lancio il messaggio
   }
   for(int i = 0; i < _order.NumPizzas();i++){ //altrimenti ciclo sugli ordini e
       // aggiungo l'ordine e il prezzo di ogni pizza dell'ordine alla stringa receipt
       receipt += "- " + _order.Pizzas[i].Name + ", " + to_string(_order.Pizzas[i].ComputePrice()) + " euro\n";
   }
   //aggiungo alla stringa receipt il costo totale dell'ordine
   receipt += "  TOTAL: " + to_string(_order.ComputeTotal()) + " euro\n";
   return receipt;
}

string Pizzeria::ListIngredients() const
{
    unsigned int i=0;
    unsigned int n = ingredients.size();
    string tmp[n]; //variabili temporanee
    string tmp1[n];
    int t[n];
    string a,b;
    int c;
    string fullist = "";
    for(i=0;i<n;i++){ //inizializzo le variabili temporanee a stringhe vuote
        tmp[i]="";
        tmp1[i]="";
        t[i]=0;
    }
    for(i=0;i<n;i++){ //inizializzo il vettore temporaneo di ingredienti
        tmp[i] = ingredients[i].Name;
        tmp1[i] = ingredients[i].Description;
        t[i] = ingredients[i].Price;
    }

    //riordino il vettore temporaneo degli ingredienti rispetto al nome (ordine alfabetico)
    for(unsigned int j=0;j<n-1;j++){
        for(i=0;i<n-1;i++){
            if(tmp[i]>tmp[i+1]){
                a = tmp[i];
                tmp[i] = tmp[i+1];
                tmp[i+1] = a;

                b = tmp1[i];
                tmp1[i] = tmp1[i+1];
                tmp1[i+1] = b;

                c = t[i];
                t[i] = t[i+1];
                t[i+1] = c;
            }
        }
    }

    for(i=0;i<n;i++){
        //per ogni ingrediente,aggiungo questo alla stringa fullist
        fullist += tmp[i] + " - '" + tmp1[i] + "': " + to_string(t[i]) + " euro" + "\n";
    }

    return fullist;
}

string Pizzeria::Menu() const
{
    // si riporta il formato del menu
    string fullmenu = "";
    for(int i = pizzas.size() -1;i >= 0;i--){
        fullmenu += pizzas[i].Name + " (" + to_string(pizzas[i].NumIngredients()) + " ingredients): " + to_string(pizzas[i].ComputePrice()) + " euro" + "\n";
    }
    return fullmenu;//ritorno il menu
}

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    Ingredients.push_back(ingredient); // funzione che permette di aggiungere un ingrediente al vettore di ingredienti
}

int Pizza::NumIngredients() const
{
    return Ingredients.size(); //funzione che ritorna la dimensione del vettore degli ingredienti
}

int Pizza::ComputePrice() const
{
    //funzione che calcola il costo totale degli ingredienti che sono presenti nel vettore degli ingredienti
    int total = 0;
    for(int i = 0;i < NumIngredients();i++){
        total += Ingredients[i].Price;
    }
    return total;
}

void Order::AddPizza(const Pizza &pizza)
{
    Pizzas.push_back(pizza); // funzione che permette di aggiungere una pizza al vettore di pizze
}

const Pizza &Order::GetPizza(const int &position) const
{
    unsigned int n = position;
    if(n < Pizzas.size()){ //se l'indice della posizione e' < della taglia del vettore delle pizze allora:
        return Pizzas[position];// ritorno la pizza in posizione position
    }
    throw runtime_error("Position not found");//altrimenti lancio il messaggio
}

int Order::NumPizzas() const
{
    return Pizzas.size(); // funzione che ritorna la dimensione del vettore delle pizze in un ordine
}

int Order::ComputeTotal() const
{
    //funzione che calcola il costo totale delle pizze presenti in un ordine
    int total = 0;
    for(int i = 0;i < NumPizzas();i++){
        total += Pizzas[i].ComputePrice();
    }
    return total;
}

}

#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstring>
#include <list>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
  public:
    public:
      string Name;
      int Price;
      string Description;
      Ingredient(const string& name, const string& description, const int& price);
      const string toString() const;
  };

  class Pizza {
    public:
      string Name;
      vector<Ingredient> Ingredients; //creo vettore di ingredienti

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {
    public:
      int numOrder;
      vector<Pizza> Pizzas; //creo cettore di pizze
      void InitializeOrder(int numPizzas) { throw runtime_error("Unimplemented method"); }
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const ;
      int NumPizzas() const ;
      int ComputeTotal() const ;
  };

  class Pizzeria {
  public:
      vector<PizzeriaLibrary::Ingredient> ingredients; //creo vettore di ingredienti
      vector<PizzeriaLibrary::Pizza> pizzas; //creo vettore di pizze
      vector<PizzeriaLibrary::Order> orders; //creo vettore di ordini
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const ;
      int CreateOrder(const vector<string>& pizzas) ;
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const ;
      string ListIngredients()const  ;
      string Menu() const ;
  };
};

#endif // PIZZERIA_H

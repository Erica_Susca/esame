class Ingredient:
    def __init__(self, name: str, price: int, description: str): #costruttore della classe ingrediente con nome, prezzo e descrizione
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str): # costruttore della classe pizza con nome ed ingredienti
        self.Name = name
        # tutti gli ingredienti vengono raccolti in una lista
        self.Ingredients = []

    #funzione che aggiunge gli ingredienti
    def addIngredient(self, ingredient: Ingredient):
        # la funzione append permette di inserire gli elementi in coda ad una lista
        self.Ingredients.append(ingredient)

    # questo metodo restituisce il numero degli elementi
    def numIngredients(self) -> int:
        # la funzione len restituisce il numero di ingredienti nella lista
        return len(self.Ingredients)

    # questo metodo calcola il prezzo totale degli ingredienti nella lista
    def computePrice(self) -> int:
        price = 0
        for i in range(0, self.numIngredients()):
            price += self.Ingredients[i].Price
        return price


class Order:
    def __init__(self): # costruttore della classe degli ordini
        # le pzze vengono inserite in una lista
        self.Pizzas = []

    def getPizza(self, position: int) -> Pizza:
        if position < self.numPizzas(): #se l'indice della posizione e' < della taglia della lista delle pizze allora:
            return self.Pizzas[position] #funzione che ritorna la pizza alla posizione position
        raise RuntimeError("Position not found")

    #inizializzo il numero dell'ordine se l'ordine non e' vuoto
    def initializeOrder(self, numPizzas: int):
        if numPizzas == 0:
            raise ValueError("Empty order")

    def addPizza(self, pizza: Pizza):
        # questa funzione ci permette di aggiungere una pizza in coda alla lista
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        # restituisco il numero di pizze della lista delle pizze
        return len(self.Pizzas)

    # calcolo il prezzo totale dell'ordine
    def computeTotal(self) -> int:
        total = 0
        # mediante un ciclo for si calcola il prezzo totale di tutte le pizze
        for i in range(0, self.numPizzas()):
            total += self.Pizzas[i].computePrice()
        return total


class Pizzeria:
    def __init__(self): #costruttore della classe pizzeria
        self.AllIngredients = []
        self.AllPizzas = []
        self.AllOrders = []

    def addIngredient(self, name: str, description: str, price: int):
        try:
            self.findIngredient(name) #ricerca del nome dell'ingrediente
        except Exception:
            # se non ho trovato alcuna corrispondenza allora
            # aggiungo l'ingrediente al mio vettore di ingredienti
            self.AllIngredients.append(Ingredient(name, price, description))
            self.AllIngredients.sort(key=lambda x: x.Name, reverse=False)
            #la funzione sort riordina la lista di tutti gli ingredienti in base al nome
            return
        raise RuntimeError("Ingredient already inserted") #lancio messaggio se l'ingrediente e' gia' stato inserito

    #funzione che cerca l'ingrediente in base al nome nella lista di tutti gli ingredienti
    def findIngredient(self, name: str) -> Ingredient:
        for i in range(0, len(self.AllIngredients)):
            if self.AllIngredients[i].Name == name:
                return self.AllIngredients[i] #ritorno l'ingrediente i-esimo
        raise RuntimeError("Ingredient not found") #lancio messaggio se non ho trovato l'ingrediente

    #funzione che aggiunge la nuova pizza alla lista delle pizze
    def addPizza(self, name: str, ingredients: []):
        try:
            self.findPizza(name)
        except Exception:
            newPizza = Pizza(name)
            for i in range(0, len(ingredients)):
                self.findIngredient(ingredients[i])
            for i in range(0, len(ingredients)):
                x = self.findIngredient(ingredients[i]) #ogni volta che trovo un ingrediente
                #aggiungo l'ingrediente trovato alla lista degli ingredienti che servono per creare la pizza
                newPizza.Ingredients.append(x)
            # tramite la funzione apppend aggiungo la nuova pizza alla lista delle pizze
            self.AllPizzas.append(newPizza)
            return
        raise RuntimeError("Pizza already inserted") #se ho trovato una corrsipondenza per il nome della pizza lancio il messaggio

    # funzione che cerca la pizza in base al nome nella lista di tutte le pizze
    def findPizza(self, name: str) -> Pizza:
        for i in range(0, len(self.AllPizzas)):
            if self.AllPizzas[i].Name is name:
                return self.AllPizzas[i] #ritorno la pizza i-esima
        raise RuntimeError("Pizza not found") #lancio messaggio se non ho trovato la pizza

    def createOrder(self, pizzas: []) -> int:
        n = len(pizzas)
        if n == 0:
            raise RuntimeError("Empty order")
        else:
            newOrder = Order()
            newOrder.Pizzas = [self.findPizza(pizzas[i]) for i in range(0, len(pizzas))] #ogni volta che trovo una pizza
            #aggiungo la pizza trovata alla lista delle pizze che servono per creare l'ordine
            self.AllOrders.append(newOrder) #aggiungo il nuovo ordine al vettore di ordini
        return 999 + len(self.AllOrders) #il numero di ordini e' dato dalla lunghezza del vettore orders + 999
        #poiche' gli ordini partono da 1000

    #funzione che ricerca l'ordine dato il numero dell'ordine
    def findOrder(self, numOrder: int) -> Order:
        n = numOrder - 1000
        if n < len(self.AllOrders):
            return self.AllOrders[n]
        raise RuntimeError("Order not found")

    #funzione che aggiunge l'ordine e il prezzo di ogni pizza dell'ordine alla stringa receipt
    def getReceipt(self, numOrder: int) -> str:
        forder = Order()
        try:
            forder = self.findOrder(numOrder)
        except Exception:
            raise RuntimeError("Order not found")

        receipt = ""
        # riporto il formato necessario per l'output
        for i in range(0, forder.numPizzas()):
            receipt += "- " + forder.Pizzas[i].Name + ", " + str(forder.Pizzas[i].computePrice()) + " euro\n"
        receipt += "  TOTAL: " + str(forder.computeTotal()) + " euro\n"
        return receipt

    #funzione che aggiunge nome,descrizione e prezzo dipgni ingrediente alla stringa fullist piu' il costo totale per ogni pizza
    def listIngredients(self) -> str:
        fullist = ""
        # riporto il formato necessario per l'output
        for i in range(0, len(self.AllIngredients)):
            fullist += self.AllIngredients[i].Name + " - '" + self.AllIngredients[i].Description + "': " + \
                       str(self.AllIngredients[i].Price) + " euro\n"
        return fullist


    #funzione che aggiunge al menu tutte le pizze alla stringa fullmenu piu' il costo totale dell'ordine
    def menu(self) -> str:
        fullmenu = ""
        # si riporta il formato del menu
        for i in range(0, len(self.AllPizzas)):
            fullmenu += self.AllPizzas[i].Name + " (" + str(self.AllPizzas[i].numIngredients()) + " ingredients): " + \
                        str(self.AllPizzas[i].computePrice()) + " euro" + "\n"
        return fullmenu

import numpy as np
import scipy as sp
from scipy.linalg import lu_factor,lu_solve,qr,solve
### OBIETTIVO: risolvere 3 sistemi lineari utilizzando per ciascuno le fattorizzazioni PALU e QR

# \brief Test the real solution of system Ax = b
# \return the relative error for PALU solver
# \return the relative error for QR solver
def testSolution(A, b, solution):
    solPA=solveSystemPALU(A,b)
    solQR=solveSystemQR(A,b)
    errRelPA=np.linalg.norm(solution-solPA)/np.linalg.norm(solution) #calcolo l'errore relativo alla decomposizione PALU
    errRelQR=np.linalg.norm(solution-solQR)/np.linalg.norm(solution) #calcolo l'errore relativo alla decomposizione QR
    return float(errRelPA), float(errRelQR)


# \brief Solve linear system with PALU
# \return the solution
def solveSystemPALU(A, b):
    dim=A.shape
    solPA = np.zeros((dim))
    lu, piv = sp.linalg.lu_factor(A)
    solPA = sp.linalg.lu_solve((lu, piv), b) #questa funzione risolve il sistema, data la fattorizzazione LU
    return solPA


# \brief Solve linear system with PALU
# \return the solution
def solveSystemQR(A, b):
    dim = A.shape
    solQR = np.zeros((dim))
    q, r = sp.linalg.qr(A)
    y = np.dot(q.T, b)
    solQR = sp.linalg.solve(r, y) #questa funzione risolve il sistema, data la fattorizzazione QR
    return solQR

# inizializzo matrici e vettori per ogni sistema,poi risolvo il sistema e calcolo i rispettivi errori associati
if __name__ == '__main__':
    solution = [-1.0, -1.0]  #inizializzo il vettore delle soluzioni che è già noto
    n = 2
    A1 = np.zeros((n,n))
    b1 = np.zeros((n,1))
    A1[0] = [5.547001962252291e-01, -3.770900990025203e-02]
    A1[1] = [8.320502943378437e-01, -9.992887623566787e-01]
    b1[0] = [-5.169911863249772e-01]
    b1[1] = [1.672384680188350e-01]
    [errRel1PALU, errRel1QR] = testSolution(A1, b1, solution)
    if errRel1PALU < 1e-15 and errRel1QR < 1e-15:
        print("1 - PALU: {:.4e}, QR: {:.4e}".format(errRel1PALU, errRel1QR))
    else:
        print("1 - Wrong system solution found")
        exit(-1)
    A2 = np.zeros((n, n))
    b2 = np.zeros((n, 1))
    A2[0] = [5.547001962252291e-01, -5.540607316466765e-01]
    A2[1] = [8.320502943378437e-01, -8.324762492991313e-01]
    b2[0] = [-6.394645785530173e-04]
    b2[1] = [4.259549612877223e-04]
    [errRel2PALU, errRel2QR] = testSolution(A2, b2, solution)
    if errRel2PALU < 1e-12 and errRel2QR < 1e-12:
        print("2 - PALU: {:.4e}, QR: {:.4e}".format(errRel2PALU, errRel2QR))
    else:
        print("2 - Wrong system solution found")
        exit(-1)
    A3 = np.zeros((n, n))
    b3 = np.zeros((n, 1))
    A3[0] = [5.547001962252291e-01, -5.547001955851905e-01]
    A3[1] = [8.320502943378437e-01, -8.320502947645361e-01]
    b3[0] = [-6.400391328043042e-10]
    b3[1] = [4.266924591433963e-10]
    [errRel3PALU, errRel3QR] = testSolution(A3, b3, solution)
    if errRel3PALU < 1e-5 and errRel3QR < 1e-5:
        print("3 - PALU: {:.4e}, QR: {:.4e}".format(errRel3PALU, errRel3QR))
    else:
        print("3 - Wrong system solution found")
        exit(-1)

    exit(0)
#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      //costruttori della classe punto
      Point() {X = 0.0; Y = 0.0; }
      Point(const double& x,
            const double& y) { X = x; Y = y;}
      Point(const Point& point) { X = point.X; Y = point.Y; }

      double ComputeNorm2() const ;

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "";
        stream << "Point: x = " << point.X << " y = " << point.Y << endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter()<rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() < lhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() <= lhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      //costruttori della classe ellisse
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b) { _a=a; _b=b; }
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { }
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      //costruttori della classe cerchio
      Circle() { }
      Circle(const Point& center,
             const double& radius) ;
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points; //vettore di punti

    public:
      //costruttori della classe triangolo
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) { points.push_back(point);}
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      //costruttore della classe triangolo equilatero
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;//vettore di punti

    public:
      //costruttori della classe quadrilatero
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) { points.push_back(p); }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      //costruttori della classe rettangolo
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) ;
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      //costruttori della classe quadrato
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) ;
      Square(const Point& p1,
             const double& edge) ;
      virtual ~Square() { }
  };
}

#endif // SHAPE_H

#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt(0.5*(_a*_a + _b*_b)); //calcolo perimetro ellisse
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      //aggiungo al vettore di punti points i 3 punti del triangolo
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    //calcolo le lunghezze dei lati del triangolo e
    double n1 = (points[1]-points[0]).ComputeNorm2();
    double n2 = (points[2]-points[1]).ComputeNorm2();
    double n3 = (points[2]-points[0]).ComputeNorm2();
    //infine calcolo il perimetro
    perimeter += n1+n2+n3;
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
     //uso la funzione AddVertex() per aggiungere al vettore di punti, i punti del triangolo equilatero
      //sfruttando le caratteristiche del triangolo equilatero
     AddVertex(p1);
     AddVertex(Point (p1.X + (edge),p1.Y));
     AddVertex(Point (p1.X + 0.5*edge,p1.Y+(sqrt(3)/2)*edge));

  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      //aggiungo al vettore di punti points i 4 punti del quadrilatero
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);

  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    //calcolo le lunghezze dei lati del quadrilatero e
    double n1 = (points[1]-points[0]).ComputeNorm2();
    double n2 = (points[2]-points[1]).ComputeNorm2();
    double n3 = (points[3]-points[2]).ComputeNorm2();
    double n4 = (points[3]-points[0]).ComputeNorm2();
    //infine calcolo il perimetro
    perimeter += n1+n2+n3+n4;
    return perimeter;
  }

  Rectangle::Rectangle(const Point &p1, const Point &p2, const Point &p3, const Point &p4):Quadrilateral(p1,p2,p3,p4)
  {
    //costruttore della classe rettangolo
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      //uso la funzione AddVertex() per aggiungere al vettore di punti, i punti del rettangolo
       //sfruttando le caratteristiche del rettangolo
      AddVertex(p1);
      AddVertex(Point (p1.X + base,p1.Y));
      AddVertex(Point (p1.X + base,p1.Y + height));
      AddVertex(Point (p1.X,p1.Y + height));

  }

  double Point::ComputeNorm2() const
  {
      return sqrt(X*X+Y*Y);//funzione che calcola la norma di un vettore dato un punto
  }

  Point Point::operator+(const Point& point) const
  {
      return Point(X+point.X , Y+point.Y);//implemento l'operatore + di somma tra punti
  }

  Point Point::operator-(const Point& point) const
  {
      return Point(X-point.X , Y-point.Y);//implemento l'operatore - di differenza tra punti
  }

  Point&Point::operator-=(const Point& point)
  {
      X = X-point.X;
      Y = Y-point.Y;
      return *this; //implemento l'operatore - di differenza tra punti e restituisce la referenza al punto trovato
  }

  Point&Point::operator+=(const Point& point)
  {
      X = X+point.X;
      Y = Y+point.Y;
      return *this; //implemento l'operatore + di somma tra punti e restituisce la referenza al punto trovato
  }

  Circle::Circle(const Point &center, const double &radius):Ellipse(center,radius,radius)
  {
    //costruttore della classe cerchio
  }

  Square::Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4):Rectangle(p1,p2,p3,p4)
  {
    //costruttore della classe quadrato
  }

  Square::Square(const Point &p1, const double &edge)
  {
      //uso la funzione AddVertex() per aggiungere al vettore di punti, i punti del quadrato
       //sfruttando le caratteristiche del quadrato
      AddVertex(p1);
      AddVertex(Point (p1.X + edge,p1.Y));
      AddVertex(Point (p1.X + edge,p1.Y + edge));
      AddVertex(Point (p1.X,p1.Y + edge));
  }

}

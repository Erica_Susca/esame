#ifndef __TEST_SHAPE_H
#define __TEST_SHAPE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include "shape.h"

using namespace testing;
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

TEST(TestPoint, TestDefaultConstructorAndCout)
{

  ShapeLibrary::Point point;

  ostringstream streamPoint;
  streamPoint << "Point: x = " << point.X << " y = " << point.Y << endl;
  stringstream buffer;
  // Save cout's buffer here
  streambuf *sbuf = cout.rdbuf();
  // Redirect cout to our stringstream buffer or any other ostream
  cout.rdbuf(buffer.rdbuf());
  // Use cout as usual
  cout << point;
  EXPECT_EQ(buffer.str(), streamPoint.str());
  // When done redirect cout to its old self
  cout.rdbuf(sbuf);
  cout << fflush;
}

TEST(TestPoint, TestCopyConstructor)
{

  ShapeLibrary::Point center = ShapeLibrary::Point(fRand(1,2), fRand(2,4));

  ShapeLibrary::Point center2 = center;

  EXPECT_TRUE(&center != &center2);
  EXPECT_EQ(center.X, center2.X);
  EXPECT_EQ(center.Y, center2.Y);
}
TEST(TestPoint, TestOperator)
{
  ShapeLibrary::Point center = ShapeLibrary::Point(fRand(1,2), fRand(2,4));
  ShapeLibrary::Point center2 = center;

  ShapeLibrary::Point center3 = center + center2;
  ShapeLibrary::Point center4 = center - center2;

  EXPECT_TRUE(&center != &center2);
  EXPECT_EQ(center3.X, center2.X+center.X);
  EXPECT_EQ(center3.Y, center2.Y+center.Y);
  EXPECT_EQ(center4.X, center.X-center2.X);
  EXPECT_EQ(center4.Y, center.Y-center2.Y);
  center -= center2;
  EXPECT_TRUE(center.X < 1.0E-16);
  EXPECT_TRUE(center.Y < 1.0E-16);
}
TEST(TestPoint, TestStreamOperator)
{
  ShapeLibrary::Point center = ShapeLibrary::Point(fRand(1,2), fRand(2,4));

  EXPECT_TRUE(&center != NULL);
}

TEST(TestCircle, TestPerimeter)
{
  ShapeLibrary::Point center = ShapeLibrary::Point(1.2, 2.5);
  double rand = fRand(1, 100);
  double perimeter = ShapeLibrary::Circle(center, rand).Perimeter();
  double value = 2*M_PI*rand;
  EXPECT_TRUE(fabs(ShapeLibrary::Circle(center,
                                        rand).Perimeter() - 2*M_PI*rand) < 1e-6);
}

TEST(TestShape, TestPerimeter)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(2.8, 3.9);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(3.9, 1.8);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(3.1, 2.7);

  double perimeter = ShapeLibrary::Triangle(p1, p2, p3).Perimeter();
  EXPECT_DOUBLE_EQ(perimeter, 4.811745063790466);
}

TEST(TestTriangle, TestPerimeter)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(1.2, 1.7);
  double rand = fRand(1, 100);
  EXPECT_TRUE(abs(ShapeLibrary::TriangleEquilateral(p1, rand).Perimeter()-rand*3)< 1e-6);
}

TEST(TestQuadrilateral, TestPerimeter)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(-3.0, 1.0);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(1.0, -2.0);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(3.0, 2.0);
  ShapeLibrary::Point p4 = ShapeLibrary::Point(-1.0, 4.0);


  EXPECT_DOUBLE_EQ(ShapeLibrary::Quadrilateral(p1,
                                               p2,
                                               p3,
                                               p4).Perimeter(), 17.549823185463147);
}


TEST(TestRectangle, TestPerimeter)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);
  double rand = fRand(1, 100);
  double rand2 = fRand(1, 100);

  double perimeter = ShapeLibrary::Rectangle(p1, rand, rand2).Perimeter();
  EXPECT_TRUE(fabs(perimeter - 2*(rand+rand2)) < 1e-6);
}

TEST(TestSquare, TestPerimeter)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);
  double rand = fRand(1, 100);
  EXPECT_TRUE(fabs(ShapeLibrary::Square(p1,rand).Perimeter() - rand*4) < 1e-6);
}

TEST(TestPolygon, TestCompare)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(7.0, 1.0);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(7.0, 7.0);
  ShapeLibrary::Point p4 = ShapeLibrary::Point(3.0, 7.0);
  ShapeLibrary::Point p1_2 = ShapeLibrary::Point(3.0, 1.0);
  ShapeLibrary::Point p2_2 = ShapeLibrary::Point(7.0, 1.0);
  ShapeLibrary::Point p3_2 = ShapeLibrary::Point(7.0, 5.0);
  ShapeLibrary::Point p4_2 = ShapeLibrary::Point(3.0, 5.0);
  ShapeLibrary::Rectangle rectangle;
  ShapeLibrary::Square square;
  rectangle.AddVertex(p1);
  rectangle.AddVertex(p2);
  rectangle.AddVertex(p3);
  rectangle.AddVertex(p4);
  square.AddVertex(p1_2);
  square.AddVertex(p2_2);
  square.AddVertex(p3_2);
  square.AddVertex(p4_2);

  EXPECT_TRUE(rectangle > square);
  EXPECT_FALSE(rectangle < square);
}
#endif // __TEST_SHAPE_H
